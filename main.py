import os
import shutil

def cat_mapper(cat):
    blouse_sub_cats = [
        'tshirts',
        'shirts'
    ]

    pants_sub_cats = [
        'shorts,'
        'jeans',
        'trousers',
        'stockings'
    ]

    coats_sub_cats = [
        'jackets',
        'waistcoat'
    ]

    result = cat

    if cat.lower() in blouse_sub_cats:
        result = 'blouse'

    if cat.lower() in pants_sub_cats:
        result = 'pants'

    if cat.lower() in coats_sub_cats:
        result = 'coats'

    return result


def main():
    source = './dataset'
    destination = './classified_dataset'
    if not os.path.isdir(destination):
        os.mkdir(destination)
    predefined_categories = [
        'dresses',
        'skirts',
        'blouse',
        'tops',
        'pants',
        'coats',
        'others'
    ]

    files_sum = [
        0,
        0,
        0,
        0,
        0,
        0,
        0
    ]

    for category in predefined_categories:
        path = '{parent_dir}/{cat_dir}'.format(parent_dir=destination, cat_dir=category)
        if not os.path.isdir(path):
            os.mkdir(path)

    csv = open('styles.csv', 'r').read().split('\n')
    headers = csv[0].split(',')

    for line in csv[1:-1]:
        id = int(line.split(',')[headers.index('id')])
        article_type = cat_mapper(line.split(',')[headers.index('articleType')])

        try:
            files_sum[predefined_categories.index(article_type.lower())] += 1
            path = '{parent_dir}/{cat_dir}'.format(parent_dir=destination, cat_dir=article_type)

        except ValueError:
            files_sum[-1] += 1
            path = '{}/others'.format(destination)

        try:
            shutil.copy('{source_dir}/{file}.jpg'.format(source_dir=source, file=id),
                        '{destination_dir}/{file}.jpg'.format(destination_dir=path, file=id))

        except FileNotFoundError:
            print('error in {}'.format(id))

    print(files_sum)


main()
